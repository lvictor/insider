# Welcome to Insider

## Introduction
Insider allows to graphically displays large binary sample files like waveforms or sounds.

Files are opened and interpreted as a list of discrete binary samples acquired synchronously overtime. In insider's representation, the X axis represents the sample number while the Y axis represents the value of the sample (in time).

## Rationale
A large binary file typically contains more samples than there are pixels on the X axis of a computer screen. insider is built to offer multiple methods for choosing how the displayed dot is elected among the many samples it represents.

insider is designed to work on multiple platforms and to be fast and efficient event when working with multiple large opened files. It has also been designed with the ability to read different samples size and endianess.

insider is not designed to achieve a highly precise display of the data contained in the files. It is meant for visualization not for measurement. Efforts have been made however to actually achieve a reasonable (yet not professional) precision.

## Screenshots
You will see below a few screenshots of the application. These should hopefully give you a better idea of what it looks like but please note however that the GUI is highly configurable and all the views can be arranged and grouped as wished thus adapting any user's habits and needs:

![main screen](https://lh5.googleusercontent.com/-O1U2Vnl5jDI/Twxa6QRtl9I/AAAAAAAAAjY/IK5NBXwfa_c/w1279-h765-no/curves.jpg)

![main screen](https://lh6.googleusercontent.com/-AL81qKkXP1s/Twxa4WYwOZI/AAAAAAAAAjQ/GYGZVsGnwoA/s720/mainscreen.jpg)

# How do I get set up?

Please see the [WIKI](https://bitbucket.org/lvictor/insider/wiki/) for more information about

* [Building and installing](https://bitbucket.org/lvictor/insider/wiki/Building)

* [Using Insider](https://bitbucket.org/lvictor/insider/wiki/User%20Guide)

For any other enquiry, please contact the author.