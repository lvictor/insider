piccolo2dxy is a patch that can be applied on piccolo2d to add assymetric manipulations of the zoom level.
piccolo2dxy enhances the API with new methods that lets you manipulate the zoom level separately on teh X and Y axis.

For more information about piccolo2d, please visit http://www.piccolo2d.org/

HOW TO APPLY THE PATCH ?
------------------------
You must apply the patch to the original piccolo2d source code.

