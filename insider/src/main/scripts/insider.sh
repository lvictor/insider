#!/bin/sh

# Set this to the correct path if it does not work out of the box
export INSIDER_HOME=`dirname $0`

echo Using Home Dir : ${INSIDER_HOME}

java -Djava.util.logging.config.file="${INSIDER_HOME}/config/logging.properties" -jar "${INSIDER_HOME}/lib/insider-1.0-SNAPSHOT.jar"
