@echo off

REM Set this to the correct path if it does not work out of the box
set INSIDER_HOME=.

title Insider
echo Using Home Dir : %INSIDER_HOME%

java -Djava.util.logging.config.file="%INSIDER_HOME%\config\logging.properties" -jar "%INSIDER_HOME%\lib\insider-1.0-SNAPSHOT.jar"