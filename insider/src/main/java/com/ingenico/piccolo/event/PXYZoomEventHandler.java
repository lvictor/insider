package com.ingenico.piccolo.event;

import java.awt.event.InputEvent;

import edu.umd.cs.piccolo.PCamera;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.event.PZoomEventHandler;

/**
 * This class is a mix between the old deprecated PXZoomEventHandler and PYZoomEventHandler
 * 
 * @author lvictor
 *
 */
public class PXYZoomEventHandler extends PZoomEventHandler {
	@Override
	protected void dragActivityStep(PInputEvent aEvent) {
		final int eventModifiers = aEvent.getModifiers();
		final PCamera camera = aEvent.getCamera();
		
		// When CTRL_MASK is cleared then ctrl key was not down
		if ((eventModifiers & InputEvent.CTRL_MASK) == 0) {
			double dx = aEvent.getCanvasPosition().getX() - getMousePressedCanvasPoint().getX();
			double scaleDelta = (1.0 + (0.001 * dx));

			double currentScale = camera.getViewScale();
			double newScale = currentScale * scaleDelta;

			if (newScale < minScale) {
				scaleDelta = minScale / currentScale;
			}
			if ((maxScale > 0) && (newScale > maxScale)) {
				scaleDelta = maxScale / currentScale;
			}

			camera.scaleViewAboutPoint(scaleDelta, 1, viewZoomPoint.getX(), viewZoomPoint.getY());
		} else {
			double dy = aEvent.getCanvasPosition().getY() - getMousePressedCanvasPoint().getY();
			double scaleDelta = (1.0 + (0.001 * dy));

			double currentScale = camera.getViewScale();
			double newScale = currentScale * scaleDelta;

			if (newScale < minScale) {
				scaleDelta = minScale / currentScale;
			}
			if ((maxScale > 0) && (newScale > maxScale)) {
				scaleDelta = maxScale / currentScale;
			}

			camera.scaleViewAboutPoint(1, scaleDelta, viewZoomPoint.getX(), viewZoomPoint.getY());
		}
//		camera.scaleViewAboutPoint(scaleXDelta, 1, viewZoomPoint.getX(), viewZoomPoint.getY());
	}
}
