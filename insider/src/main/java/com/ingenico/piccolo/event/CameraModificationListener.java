package com.ingenico.piccolo.event;

import java.util.EventListener;

public interface CameraModificationListener extends EventListener {
	void sizeChange(CameraModificationEvent e);
	void positionChange(CameraModificationEvent e);
	void scaleChange(CameraModificationEvent e);
}
