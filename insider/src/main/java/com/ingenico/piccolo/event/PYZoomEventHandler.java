package com.ingenico.piccolo.event;

import edu.umd.cs.piccolo.PCamera;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.event.PZoomEventHandler;

@Deprecated
public class PYZoomEventHandler extends PZoomEventHandler {
	@Override
	protected void dragActivityStep(PInputEvent aEvent) {
		System.out.println("ploufY l'event: " + aEvent.getModifiers());
		PCamera camera = aEvent.getCamera();
		double dy = aEvent.getCanvasPosition().getY() - getMousePressedCanvasPoint().getY();
		double scaleDelta = (1.0 + (0.001 * dy));

		double currentScale = camera.getViewScale();
		double newScale = currentScale * scaleDelta;

		if (newScale < minScale) {
			scaleDelta = minScale / currentScale;
		}
		if ((maxScale > 0) && (newScale > maxScale)) {
			scaleDelta = maxScale / currentScale;
		}

		camera.scaleViewAboutPoint(1, scaleDelta, viewZoomPoint.getX(), viewZoomPoint.getY());
	}
}
