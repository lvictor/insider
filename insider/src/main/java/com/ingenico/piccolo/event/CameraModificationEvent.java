package com.ingenico.piccolo.event;

import java.util.EventObject;

public class CameraModificationEvent extends EventObject {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 5194987165723589033L;

	private Object newValue;
	private Object oldValue;

	public CameraModificationEvent(Object source, Object oldValue, Object newValue) {
		super(source);
		this.oldValue   = oldValue;
		this.newValue   = newValue;
	}

    public Object getNewValue() {
		return newValue;
	}

	public Object getOldValue() {
		return oldValue;
	}

	/**
     * Returns a String representation of this EventObject.
     *
     * @return  A a String representation of this EventObject.
     */
    public String toString() {
        return getClass().getName() + "GeometryChangeEvent [source=" + source + ", oldValue=" + oldValue + ", newValue=" + newValue + "]";
    }
}
