package com.ingenico.piccolo.event;

import com.ingenico.insider.nodes.PCurvePath;
import com.ingenico.insider.nodes.PMark;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.event.PDragEventHandler;
import edu.umd.cs.piccolo.event.PInputEvent;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PDimension;

public class PCurveDragEventHandler extends PDragEventHandler {
	@Override
	protected void drag(PInputEvent event) {
		PNode draggedNode = getDraggedNode();
		if (draggedNode instanceof PCurvePath) {
			draggedNode = draggedNode.getParent().getParent();
		}
		PDimension d = event.getDeltaRelativeTo(draggedNode);		
		draggedNode.localToParent(d);

		if (draggedNode instanceof PMark) {
			// Some nodes shall not be offset but just moved... 
			final PBounds draggedNodeBounds = draggedNode.getBoundsReference();
			draggedNode.setBounds(
				draggedNodeBounds.getX() + d.getWidth(),
				draggedNodeBounds.getY() + d.getHeight(), 
				draggedNodeBounds.getWidth(),
				draggedNodeBounds.getHeight()
			);
		} else {
			draggedNode.offset(d.getWidth(), d.getHeight());
		}
	}

}
