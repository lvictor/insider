package com.ingenico.insider.nodes;

import com.ingenico.insider.services.impl.LocalisationSupplier;
import com.ingenico.insider.util.Constants;

import edu.umd.cs.piccolo.PNode;

public class PMark extends PNode {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 5010452603998030695L;

	/**
	 * The name that is given to this mark
	 */
	private String markName;

	public PMark() {
		markName = LocalisationSupplier.getInstance().localize(Constants.UNDEFINED_KEY);
	}

	public PMark(PMark model) {
		markName = LocalisationSupplier.getInstance().localize(Constants.UNDEFINED_KEY);
		this.setBounds(model.getBounds());
	}

	public PMark(String name) {
		this.markName = name;
	}

	public void setMarkName(String markName) {
		this.markName = markName;
	}

	public String getMarkName() {
		return markName;
	}

	@Override
	public String toString() {
		return markName;// + "[" + paramString() + "]";
	}
}
