package com.ingenico.insider.nodes;

import java.awt.Paint;
import java.util.Iterator;

import com.ingenico.piccolo.event.CameraModificationListener;
import com.ingenico.tools.data.chopping.Chopper;
import com.ingenico.tools.data.sampling.PathSampler;

import edu.umd.cs.piccolo.PNode;

public abstract class PCurve extends PNode implements CameraModificationListener {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 8062444794132418757L;

	/** 
	 * The property name that identifies a change of this node's stroke paint
	 * (see {@link #getStrokePaint getStrokePaint}). Both old and new value will
	 * be set correctly to Paint objects in any property change event.
	 */
	public static final String PROPERTY_STROKE_PAINT = "strokePaint";
	public static final int PROPERTY_CODE_STROKE_PAINT = 1 << 16;

	protected Paint strokePaint;

	protected PathSampler pathSampler;
	protected Chopper chopper;

	public Paint getStrokePaint() {
		return strokePaint;
	}

	public void setStrokePaint(Paint aPaint) {
		Paint old = strokePaint;
		strokePaint = aPaint;
		Iterator<PNode> children = getChildrenIterator();
		while (children.hasNext()) {
			PNode child = children.next();
			if (child instanceof PCurvePath) {
				((PCurvePath)child).setStrokePaint(aPaint);
			}
		}

		invalidatePaint();
		firePropertyChange(PROPERTY_CODE_STROKE_PAINT ,PROPERTY_STROKE_PAINT, old, strokePaint);
	}

	public PathSampler getPathSampler() {
		return pathSampler;
	}

	public void setPathSampler(PathSampler sampler) {
		this.pathSampler = sampler;
	}
	
	public Chopper getChopper() {
		return chopper;
	}

	public void setChopper(Chopper chopper) {
		this.chopper = chopper;
	}
}
