package com.ingenico.insider.util;

public final class Constants {
	public static final String BUILD_ID = "Build-Id";

	public static final String ENTRY  = "ENTRY";
	public static final String RETURN = "RETURN";

	public static final String SEPARATOR = ".";
	public static final String INSIDER_BASE = "com.ingenico.insider";

	public static final String TABLE_KEY = "table";
	public static final String MESSAGE_KEY = "message";
	public static final String FORMAT_KEY = "format";
	public static final String UNDEFINED_KEY = "undefined";
	public static final String BACKGROUND_KEY = "background";
	public static final String FOREGROUND_KEY = "foreground";
	public static final String STROKE_KEY = "stroke";
	public static final String TEXT_KEY = "text";

	public static final String DEFAULT_ERROR_TITLE = "Error";

	public static final String DEFAULT_INTERNAL_ERROR_FORMAT = "An internal error has occured:\n{0}";
	public static final String DEFAULT_INTERNAL_ERROR_MESSAGE = "An undefined internal error has occured.";

	/**
	 * Private constructor to forbid instantiation
	 */
	private Constants() {}
}
