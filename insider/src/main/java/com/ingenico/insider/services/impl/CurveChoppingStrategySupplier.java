package com.ingenico.insider.services.impl;

import java.util.logging.Logger;

import com.ingenico.tools.data.chopping.Chopper;
import com.ingenico.tools.data.chopping.VersatileChopper;

public final class CurveChoppingStrategySupplier {
	private static final Logger _logger = Logger.getLogger(CurveChoppingStrategySupplier.class.getCanonicalName()); 

	/**
	 * singleton instance
	 */
	private static CurveChoppingStrategySupplier instance;

	/**
	 * The list of path sampling strategies that are made available to the user 
	 */
	private static final Chopper[] availableChoppers = {
		new VersatileChopper()
	};

	/**
	 * private constructor of final class to prevent external instantiation
	 */

	private CurveChoppingStrategySupplier() {};
	/**
	 * retrieve the singleton instance
	 *
	 * @return the CurveChoppingStrategySupplier singleton
	 */
	public static CurveChoppingStrategySupplier getInstance() {
		if (instance == null) {
			_logger.info(CurveChoppingStrategySupplier.class.getName() + " instance does not exist... Creating instance.");
			instance = new CurveChoppingStrategySupplier();
		}
		return instance;
	}

	/**
	 * Gives the list of available path sampler strategies.
	 * 
	 *  These strategies can be used directly but are not garanteed to
	 * be thread safe.
	 *  Usage in threads can be achieved eihter through cloning strategies or through
	 * synchronized calls to one particular strategy.
	 *
	 * @return the list of strategies
	 */
	public Chopper[] getAvailableChoppers() {
		return availableChoppers;
	}

	/**
	 * Gives a default paths sampler strategy
	 *
	 *  These strategies can be used directly but are not garanteed to
	 * be thread safe.
	 *  Usage in threads can be achieved eihter through cloning strategies or through
	 * synchronized calls to one particular strategy.
	 *
	 * @return the default strategy
	 */
	public Chopper getDefaultStrategy() {
		return availableChoppers[0];
	}
}