package com.ingenico.insider.services.impl;

import java.util.logging.Logger;

import edu.umd.cs.piccolo.util.PPaintContext;

final class CanvasSupplierTools {
	// re-use the CanvasSupplier resource bundle
	private static final Logger _logger = Logger.getLogger(CanvasSupplier.class.getCanonicalName()); 

	static int stringToPPaintContextQuality(String string) {
		final int defaultPaintContextQuality = PPaintContext.LOW_QUALITY_RENDERING;

		if ( (string == null) || (string.isEmpty())) {
			_logger.warning("No default rendering quality defined for canvas. Using default.");
		} else if ("low".equalsIgnoreCase(string)) {
			return PPaintContext.LOW_QUALITY_RENDERING;
		} else if ("high".equalsIgnoreCase(string)) {
			return PPaintContext.HIGH_QUALITY_RENDERING;
		} else {
			_logger.warning("Unknown rendering quality \"" + string + "\". Using default.");
		}
		return defaultPaintContextQuality;
	}

	private CanvasSupplierTools() {}
}
