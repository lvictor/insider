package com.ingenico.insider.services.impl;

import java.util.logging.Logger;

import com.ingenico.insider.nodes.PNotificationArea;
import com.ingenico.insider.util.Constants;

import edu.umd.cs.piccolo.activities.PActivity;

public final class NotificationSupplier {
	private static final Logger _logger = Logger.getLogger(NotificationSupplier.class.getCanonicalName());

	/**
	 * singleton instance
	 */
	private static NotificationSupplier instance;

	/**
	 * Public constant keywords
	 */
	public static final String NOTIFICATION_KEY = "notification";
	public static final String NOTIFICATION_BASE = Constants.INSIDER_BASE + Constants.SEPARATOR + NOTIFICATION_KEY;

	public static final String TRANSPARENCY_PATH  = NOTIFICATION_BASE + Constants.SEPARATOR + "transparency";
	public static final String DISAPPEARANCE_PATH = NOTIFICATION_BASE + Constants.SEPARATOR + "disappearance";
	public static final String BACKGROUND_PATH    = NOTIFICATION_BASE + Constants.SEPARATOR + Constants.BACKGROUND_KEY;
	public static final String STROKE_PATH        = NOTIFICATION_BASE + Constants.SEPARATOR + Constants.STROKE_KEY;
	public static final String TEXT_PATH          = NOTIFICATION_BASE + Constants.SEPARATOR + Constants.TEXT_KEY;

	public static final String PROGRESSBAR_BASE = NOTIFICATION_BASE + Constants.SEPARATOR + "progress";
	public static final String PROGRESSBAR_BACKGROUND_PATH = PROGRESSBAR_BASE + Constants.SEPARATOR + Constants.BACKGROUND_KEY;
	public static final String PROGRESSBAR_FOREGROUND_PATH = PROGRESSBAR_BASE + Constants.SEPARATOR + Constants.FOREGROUND_KEY;
	
	/**
	 * The Notification area piccolo object
	 */
	private final PNotificationArea notificationArea;

	/**
	 * private constructor of final class to prevent external instantiation
	 */
	private NotificationSupplier () {
		notificationArea = new PNotificationArea(null, false);
		CanvasSupplier.getInstance().getCanvas().getCamera().addChild(notificationArea);
		initNotificationArea();
	}

	private void initNotificationArea() {
		final UserPreferencesSupplier userPreferencesSupplier = UserPreferencesSupplier.getInstance();
		notificationArea.setPaint(userPreferencesSupplier.getColor(BACKGROUND_PATH));
		notificationArea.setStrokePaint(userPreferencesSupplier.getColor(STROKE_PATH));
		notificationArea.setDefaultTransparency(userPreferencesSupplier.getFloat(TRANSPARENCY_PATH).floatValue());
		notificationArea.setDisappearanceDuration(userPreferencesSupplier.getDecimal(DISAPPEARANCE_PATH).longValue());
		notificationArea.setProgressBackgroundPaint(userPreferencesSupplier.getColor(PROGRESSBAR_BACKGROUND_PATH));
		notificationArea.setProgressDisplayPaint(userPreferencesSupplier.getColor(PROGRESSBAR_FOREGROUND_PATH));

		notificationArea.addActivity(new PActivity(-1){
			@Override
			protected void activityStep(long elapsedTime) {
				super.activityStep(elapsedTime);

				float workerLoad = AsynchronousSamplingJobProcessor.getWorkerLoad();
				if (workerLoad == 0) {
//					notificationArea.setUserText("Ready...");
					notificationArea.setProgressBarVisible(false);
					if (notificationArea.getUserText() == null) {
						notificationArea.setVisible(false);
					}
				} else {
					notificationArea.setProgressBarVisible(true);
					notificationArea.setVisible(true);
					notificationArea.setProgressBar(1-workerLoad);
				}
			}
		});
	}

	/**
	 * retrieve the singleton instance
	 *
	 * @return the NotificationSupplier singleton
	 */
	public static NotificationSupplier getInstance() {
		if (instance == null) {
			_logger.info(NotificationSupplier.class.getName() + " instance does not exist... Creating instance.");
			instance = new NotificationSupplier();
		}
		return instance;
	}

	public void setUserText(String displayedText) {
		notificationArea.setUserText(displayedText);
	}
	
	public void setVisible(boolean visible) {
		notificationArea.setVisible(visible);
	}
}
