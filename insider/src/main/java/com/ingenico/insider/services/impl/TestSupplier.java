package com.ingenico.insider.services.impl;

import java.util.logging.Logger;

import com.ingenico.insider.services.impl.ThemesMenuSupplier;

@Deprecated
public final class TestSupplier {
	private static final Logger _logger = Logger.getLogger(TestSupplier.class.getCanonicalName());
	/**
	 * singleton instance
	 */
	private static TestSupplier instance;
	
	/**
	 * private constructor of final class to prevent external instantiation
	 */
	private TestSupplier() {
//		final DockableViewsSupplier frame = DockableViewsSupplier.getInstance();

		// Do your garbage here ! 
	}

	/**
	 * retrieve the singleton instance
	 *
	 * @return the TestSupplier singleton
	 */
	public static TestSupplier getInstance() {
		if (instance == null) {
			_logger.info(ThemesMenuSupplier.class.getName() + " instance does not exist... Creating instance.");
			instance = new TestSupplier();
		}
		return instance;
	}	
}
