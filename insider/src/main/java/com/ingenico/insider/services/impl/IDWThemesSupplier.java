package com.ingenico.insider.services.impl;

import java.util.logging.Logger;

import net.infonode.docking.theme.BlueHighlightDockingTheme;
import net.infonode.docking.theme.ClassicDockingTheme;
import net.infonode.docking.theme.DefaultDockingTheme;
import net.infonode.docking.theme.DockingWindowsTheme;
import net.infonode.docking.theme.GradientDockingTheme;
import net.infonode.docking.theme.LookAndFeelDockingTheme;
import net.infonode.docking.theme.ShapedGradientDockingTheme;
import net.infonode.docking.theme.SlimFlatDockingTheme;
import net.infonode.docking.theme.SoftBlueIceDockingTheme;

public final class IDWThemesSupplier {
	private static Logger _logger = Logger.getLogger(IDWThemesSupplier.class.getCanonicalName()); 

	/**
	 * singleton instance
	 */
	private static IDWThemesSupplier instance;

	/**
	 * The list of themes that is made available to the user 
	 */
	private static final DockingWindowsTheme[] availableThemes = {
		new BlueHighlightDockingTheme(),
		new ClassicDockingTheme(),
		new DefaultDockingTheme(),
		new GradientDockingTheme(),
		new LookAndFeelDockingTheme(),
		new ShapedGradientDockingTheme(),
		new SlimFlatDockingTheme(),
		new SoftBlueIceDockingTheme()
	};

	/**
	 * private constructor of final class to prevent external instantiation
	 */
	private IDWThemesSupplier() {};

	/**
	 * retrieve the singleton instance
	 *
	 * @return the IDWThemesSupplier singleton
	 */
	public static IDWThemesSupplier getInstance() {
		if (instance == null) {
			_logger.info(IDWThemesSupplier.class.getName() + " instance does not exist... Creating instance.");
			instance = new IDWThemesSupplier();
		}
		return instance;
	}

	/**
	 * Get the list of available docking windows themes
	 *
	 * @return an array with an instance of each available docking windows theme
	 */
	public DockingWindowsTheme[] getAvailableThemes() {
		return availableThemes;
	}

	/**
	 * Returns an hardcoded default docking windows theme
	 *
	 * @return the default theme
	 */
	public DockingWindowsTheme getDefaultTheme() {
		return availableThemes[5];
	}
}
