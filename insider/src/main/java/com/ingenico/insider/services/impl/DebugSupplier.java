package com.ingenico.insider.services.impl;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Logger;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.AbstractAction;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import edu.umd.cs.piccolo.util.PDebug;

public final class DebugSupplier {
	private static final Logger _logger = Logger.getLogger(DebugSupplier.class.getCanonicalName());
	
	/**
	 * The file chooser used to load and save curves...
	 */
	private final JFileChooser scriptChooser;

	/**
	 * singleton instance
	 */
	private static DebugSupplier instance;

	/**
	 * private constructor of final class to prevent external instantiation
	 */
	private DebugSupplier() {
		JMenu debugMenu = StandardMenuSupplier.getInstance().getStandardMenu("debug");

		debugMenu.add(new JCheckBoxMenuItem(new AbstractAction("Show Bounds") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				PDebug.debugBounds = !PDebug.debugBounds;
			}
		}));

		debugMenu.add(new JCheckBoxMenuItem(new AbstractAction("Show Full Bounds") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				PDebug.debugFullBounds = !PDebug.debugFullBounds;
			}
		}));

		debugMenu.add(new JCheckBoxMenuItem(new AbstractAction("Show Region Managment") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				PDebug.debugRegionManagement = !PDebug.debugRegionManagement;
			}
		}));

		debugMenu.add(new JCheckBoxMenuItem(new AbstractAction("Print Frame Rates to Console") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				PDebug.debugPrintFrameRate = !PDebug.debugPrintFrameRate;
			}
		}));

		debugMenu.add(new JCheckBoxMenuItem(new AbstractAction("Print Used Memory to Console") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				PDebug.debugPrintUsedMemory = !PDebug.debugPrintUsedMemory;
			}
		}));

		scriptChooser = new JFileChooser();
		scriptChooser.setMultiSelectionEnabled(false);
		debugMenu.add(new JMenuItem(new AbstractAction("Eval script file...") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				int status = scriptChooser.showOpenDialog(DockableViewsSupplier.getInstance().getComponent());
				if (status == JFileChooser.APPROVE_OPTION) {
					final File selectedFile = scriptChooser.getSelectedFile();
					FileReader reader = null;
					// create a script engine manager
			        ScriptEngineManager factory = new ScriptEngineManager();
			        // create JavaScript engine
			        ScriptEngine engine = factory.getEngineByName("JavaScript");
			        // evaluate JavaScript code from given file - specified by first argument
			        try {
			        	reader = new FileReader(selectedFile);
						engine.eval(reader);
						reader.close();
					} catch (FileNotFoundException e1) {
						MessageBoxSupplier.getInstance().showErrorDialog(CurvesSupplier.NOTFOUND_MESSAGE_KEY, selectedFile);
					} catch (ScriptException se) {
						MessageBoxSupplier.getInstance().showErrorDialog(CurvesSupplier.IO_MESSAGE_KEY, se);
						try {
							reader.close();
						} catch (IOException ioe) {
							MessageBoxSupplier.getInstance().showErrorDialog(CurvesSupplier.IO_MESSAGE_KEY, ioe);
						}
					} catch (IOException ioe) {
						MessageBoxSupplier.getInstance().showErrorDialog(CurvesSupplier.IO_MESSAGE_KEY, ioe);
					}
				}
			}
		}));
//		final DockableViewsSupplier frame = DockableViewsSupplier.getInstance();

		// Do your garbage here !
	}

	/**
	 * retrieve the singleton instance
	 *
	 * @return the DebugSupplier singleton
	 */
	public static DebugSupplier getInstance() {
		if (instance == null) {
			_logger.info(ThemesMenuSupplier.class.getName() + " instance does not exist... Creating instance.");
			instance = new DebugSupplier();
		}
		return instance;
	}	
}
