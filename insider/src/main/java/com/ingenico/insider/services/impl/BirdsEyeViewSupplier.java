package com.ingenico.insider.services.impl;

import java.util.logging.Logger;

import com.ingenico.piccolo.nodes.PBirdsEyeView;

import edu.umd.cs.piccolo.PCanvas;

public final class BirdsEyeViewSupplier {
	private static final Logger _logger = Logger.getLogger(BirdsEyeViewSupplier.class.getCanonicalName()); 

	/**
	 * Public constant keywords
	 */
	public static final String BIRDS_EYE_VIEW_KEY = "bev";

	/**
	 * singleton instance
	 */
	private static BirdsEyeViewSupplier instance;

	/**
	 * The bird's eye view piccolo component
	 */
	private final PBirdsEyeView birdsEyeView;
	
	/**
	 * private constructor of final class to prevent external instantiation
	 */
	private BirdsEyeViewSupplier() {
		birdsEyeView = new PBirdsEyeView();
		initBirdsEyeView();
		DockableViewsSupplier.getInstance().add(birdsEyeView, BIRDS_EYE_VIEW_KEY);
	}

	/**
	 * Initializes the bird's eye view piccolo component and connect it to the viewed canvas
	 */
	private void initBirdsEyeView() {
		final PCanvas canvas = CanvasSupplier.getInstance().getCanvas();
		birdsEyeView.connect(canvas, canvas.getLayer());
	}

	/**
	 * retrieve the singleton instance
	 *
	 * @return the BirdsEyeViewSupplier singleton
	 */
	public static BirdsEyeViewSupplier getInstance() {
		if (instance == null) {
			_logger.info(BirdsEyeViewSupplier.class.getName() + " instance does not exist... Creating instance.");
			instance = new BirdsEyeViewSupplier();
		}
		return instance;
	}
}
