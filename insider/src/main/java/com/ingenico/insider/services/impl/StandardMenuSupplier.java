package com.ingenico.insider.services.impl;

import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.logging.Logger;

import javax.swing.JMenu;

public final class StandardMenuSupplier {
	private static Logger _logger = Logger.getLogger(StandardMenuSupplier.class.getCanonicalName()); 

	/**
	 * Public constant keywords
	 */
	public static final String FILE_KEY = "file";
	public static final String EDIT_KEY = "edit";
	public static final String DISPLAY_KEY = "display";
	public static final String WINDOWS_KEY = "windows";
	public static final String HELP_KEY = "help";

	/**
	 * singleton instance
	 */
	private static StandardMenuSupplier instance;

	/**
	 * Maps the menu key with its JMenuItem
	 */
	private static final HashMap<String, JMenu> menuTable = new HashMap<String, JMenu>();

	/**
	 * private constructor of final class to prevent external instantiation
	 */
	private StandardMenuSupplier() {};

	/**
	 * retrieve the singleton instance
	 *
	 * @return the StandardMenuSupplier singleton
	 */
	public static StandardMenuSupplier getInstance() {
		if (instance == null) {
			_logger.info(StandardMenuSupplier.class.getName() + " instance does not exist... Creating instance.");
			instance = new StandardMenuSupplier();
		}
		return instance;
	}

	/**
	 * Generic method that retrieves a menu from the standard menu table.
	 * 
	 * key parameter is used either as a key to uniquely identify the menu
	 * in the standard menu table and as the subkey used to search for the
	 * menu localized string in the menu.properties resource file.
	 * 
	 * properties keys must be prefixed by "com.ingenico.insider" therefore, the
	 * key menu.file will be searched in "com.ingenico.insider.menu.file"
	 * 
	 * @param key the menu key
	 * @return the last stored menu or a new menu if none was stored for that key.
	 * @throws MissingResourceException if the menu localized string cannot be fetched
	 */
	public JMenu getStandardMenu (String key) throws MissingResourceException {
		JMenu menu = menuTable.get(key);
		if (menu == null) {
			menu = (JMenu) LocalisationSupplier.getInstance().localize(new JMenu(), key);
			menuTable.put(key, menu);
		}
		return menu;
	}

	/**
	 * Convenience method that returns the file menu
	 * 
	 * @return the file menu
	 */
	public JMenu getFileMenu () throws MissingResourceException {
		return getStandardMenu(FILE_KEY);
	}
	
	/**
	 * Convenience method that returns the edit menu
	 * 
	 * @return the edit menu
	 */
	public JMenu getEditMenu () throws MissingResourceException {
		return getStandardMenu(EDIT_KEY);
	}

	/**
	 * Convenience method that returns the windows menu
	 * 
	 * @return the windows menu
	 */
	public JMenu getWindowsMenu () throws MissingResourceException {
		return getStandardMenu(WINDOWS_KEY);
	}

	/**
	 * Convenience method that returns the display menu
	 * 
	 * @return the display menu
	 */
	public JMenu getDisplayMenu () throws MissingResourceException {
		return getStandardMenu(DISPLAY_KEY);
	}

	/**
	 * Convenience method that returns the help menu 
	 * 
	 * @return the help menu
	 */
	public JMenu getHelpMenu () throws MissingResourceException {
		return getStandardMenu(HELP_KEY);
	}
}
