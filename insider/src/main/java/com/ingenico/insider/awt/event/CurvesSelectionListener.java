package com.ingenico.insider.awt.event;

import java.util.logging.Logger;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.ingenico.insider.nodes.PSampleChannel;
import com.ingenico.insider.services.impl.CurvePropertyControlSupplier;
import com.ingenico.insider.services.impl.CurvesControlSupplier;
import com.ingenico.insider.util.Constants;

public class CurvesSelectionListener implements ListSelectionListener {
	private static final Logger _logger = Logger.getLogger(CurvesSelectionListener.class.getCanonicalName());

	@Override
	public void valueChanged(ListSelectionEvent e) {
		_logger.finest(Constants.ENTRY);
		
		if (e.getValueIsAdjusting()) {
			_logger.finest("Value is adjusting... Nothing will be done");
			return;
		}

		final Object[] currentCurve = CurvesControlSupplier.getInstance().getSelectedValues();

		if ( currentCurve != null ) {
			final CurvePropertyControlSupplier curvesControl = CurvePropertyControlSupplier.getInstance();
			curvesControl.setVisible(true);
			switch (currentCurve.length) {
			case 0:
				_logger.finest("No Selected Item");
				CurvePropertyControlSupplier.getInstance().setVisible(false);
				break;
			case 1:
				_logger.finest("Selected Item = " + currentCurve);
				curvesControl.showPropertiesFromPSampleChannel((PSampleChannel)(currentCurve[0]));
				curvesControl.getReloadCurvesPropertiesAction().setEnabled(true);
				curvesControl.setApplyOnlySelectedLines(false);
				break;
			default:
				_logger.finest("Multiple Selection");
				curvesControl.setApplyOnlySelectedLines(true);
				curvesControl.getReloadCurvesPropertiesAction().setEnabled(false);
			}
		} else {
			_logger.finest("Null Selection");
			CurvePropertyControlSupplier.getInstance().setVisible(false);
		}
		_logger.finest(Constants.RETURN);
	}			
}
