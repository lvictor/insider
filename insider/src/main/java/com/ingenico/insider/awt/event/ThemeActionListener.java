package com.ingenico.insider.awt.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import com.ingenico.insider.services.impl.DockableViewsSupplier;
import com.ingenico.insider.services.impl.IDWThemesSupplier;

public class ThemeActionListener implements ActionListener {
	private static Logger _logger = Logger.getLogger(ThemeActionListener.class.getCanonicalName()); 

	@Override
	public void actionPerformed(ActionEvent e) {
		int themeIndex = 0;

		themeIndex = Integer.parseInt(e.getActionCommand());
		_logger.fine("Theme Menu Item selected. Restoring Theme Id=" + themeIndex);
		DockableViewsSupplier.getInstance().setTheme(IDWThemesSupplier.getInstance().getAvailableThemes()[themeIndex]);
	}
}
