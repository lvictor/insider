package com.ingenico.insider.awt.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import com.ingenico.insider.services.impl.DockableViewsSupplier;

public class ViewsMenuActionListener implements ActionListener {
	private static Logger _logger = Logger.getLogger(ViewsMenuActionListener.class.getCanonicalName()); 

	@Override
	public void actionPerformed(ActionEvent e) {
		int viewId = 0;

		viewId = Integer.parseInt(e.getActionCommand());
		_logger.fine("View Menu Item selected. Restoring Component Id=" + viewId);
		DockableViewsSupplier.getInstance().restoreComponentById(viewId);
	}
}
