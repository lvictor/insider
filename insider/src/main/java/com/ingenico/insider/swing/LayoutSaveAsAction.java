package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.LayoutControlSupplier;


public class LayoutSaveAsAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 584225719869373360L;

	@Override
	public void actionPerformed(ActionEvent e) {
		LayoutControlSupplier.getInstance().saveAs();
	}
}
