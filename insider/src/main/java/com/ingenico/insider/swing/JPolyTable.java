package com.ingenico.insider.swing;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

public class JPolyTable extends JTable {

	private static final long serialVersionUID = 1L;
	
	public JPolyTable() {
		super();
	}

	public JPolyTable(AbstractTableModel model) {
		super(model);
	}

	/**
	 * Override the default renderer to allow for polyvalent cell rendering.
	 * 
	 * The JPolyvalentTable will look for the renderer for each cell instead of just its column.
	 */
	public TableCellRenderer getCellRenderer(int row, int column) {
		Object value = getValueAt(row,column);

		if (value !=null) {
			return getDefaultRenderer(value.getClass());
		} else {
			return super.getCellRenderer(row,column);
		}
	}

	public TableCellEditor getCellEditor(int row, int column) {
		Object value = getValueAt(row,column);
		if (value != null) {
			return getDefaultEditor(value.getClass());
		} else {
			return super.getCellEditor(row,column);
		}
	}
}
