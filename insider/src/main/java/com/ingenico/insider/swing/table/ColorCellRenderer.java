package com.ingenico.insider.swing.table;
import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

/**
 * 
 * @author lvictor
 *
 */
public class ColorCellRenderer extends JLabel implements TableCellRenderer {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Border unselectedBorder = null;
    Border selectedBorder   = null;

    boolean isBordered      = true;

    public ColorCellRenderer(boolean isBordered) {
        this.isBordered = isBordered;
        setOpaque(true); // MUST do this for background to show up.
    }

    public Component getTableCellRendererComponent(final JTable table, Object color, boolean isSelected, boolean hasFocus, int row, int column) {
        Color newColor = (Color)color;

        setBackground(newColor);

        if (isBordered) {
            if (isSelected) {
                if (selectedBorder == null) {
                    selectedBorder = BorderFactory.createMatteBorder(1,1,1,1, table.getSelectionBackground());
                }
                setBorder(selectedBorder);
            } else {
                if (unselectedBorder == null) {
                    unselectedBorder = BorderFactory.createMatteBorder(1,1,1,1, table.getBackground());
                }
                setBorder(unselectedBorder);
            }
        }

        setToolTipText("RGBa value: (" + newColor.getRed()   + ", "
                                       + newColor.getGreen() + ", "
                                       + newColor.getBlue()  + "), "
                                       + newColor.getAlpha());
        return this;
    }
}
