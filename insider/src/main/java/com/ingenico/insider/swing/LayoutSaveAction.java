package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.LayoutControlSupplier;


public class LayoutSaveAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 184965825864906526L;

	@Override
	public void actionPerformed(ActionEvent e) {
		LayoutControlSupplier.getInstance().save();
	}
}
