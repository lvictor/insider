package com.ingenico.insider.swing.table;

import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

import com.ingenico.insider.services.impl.MessageBoxSupplier;

public class IntegerCellEditor extends AbstractCellEditor implements TableCellEditor {
	private static final Logger _logger = Logger.getLogger(IntegerCellEditor.class.getCanonicalName());

	/**
	 * 
	 */
	private static final long serialVersionUID = -5229420642092153103L;

	final JTextField textField;

	public IntegerCellEditor() {
		textField = new JTextField();
// TODO : validate this field.
// see http://www.iam.ubc.ca/guides/javatut99/uiswing/components/textfield.html
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		textField.setText(value.toString());
		return textField;
	}

	@Override
	public Object getCellEditorValue() {
		Integer intValue = null;
		try {
			intValue = Integer.parseInt(textField.getText());
		} catch (NumberFormatException nfe) {
			MessageBoxSupplier.getInstance().showErrorDialog(nfe.getClass().getSimpleName(), textField.getText());
			_logger.log(Level.WARNING, nfe.getMessage(), nfe);
		}

		return intValue;
	}

}
