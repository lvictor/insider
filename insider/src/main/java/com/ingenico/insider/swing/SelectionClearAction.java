package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.SelectionLayerSupplier;

public class SelectionClearAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 2938918261062861180L;

	@Override
	public void actionPerformed(ActionEvent e) {
		SelectionLayerSupplier.getInstance().clearSelection();
	}
}
