package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.CurvesControlSupplier;


public class CurveCloseAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 3826329372063442245L;

	@Override
	public void actionPerformed(ActionEvent e) {
		CurvesControlSupplier.getInstance().close();
	}
}
