package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.nodes.PSampleChannel;
import com.ingenico.insider.services.impl.CurvePropertyControlSupplier;
import com.ingenico.insider.services.impl.CurvesControlSupplier;

public class CurvesPropertiesReloadAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -3455333197401935436L;

	@Override
	public void actionPerformed(ActionEvent e) {
		final PSampleChannel currentCurve = CurvesControlSupplier.getInstance().getSelectedValue();
		CurvePropertyControlSupplier.getInstance().showPropertiesFromPSampleChannel(currentCurve);
	}
}
