package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;
import java.awt.geom.Rectangle2D;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.CanvasSupplier;

import edu.umd.cs.piccolo.PCamera;
import edu.umd.cs.piccolo.util.PAffineTransform;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PDimension;

public class ZoomWidthAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 6552452442128765042L;
	
	@Override
	public void actionPerformed(ActionEvent e) {
		final PCamera camera = CanvasSupplier.getInstance().getCanvas().getCamera(); 
		final Rectangle2D cameraLayersBounds = camera.getUnionOfLayerFullBounds();

		final PBounds cameraViewBounds = camera.getViewBounds();
		final PDimension delta = cameraViewBounds.deltaRequiredToCenter(cameraLayersBounds);
		final PAffineTransform newTransform = camera.getViewTransform();

		newTransform.translate(delta.width, 0);

		final double scaleX = cameraViewBounds.getWidth() / cameraLayersBounds.getWidth();
		if (scaleX != Double.POSITIVE_INFINITY && scaleX != 0) {
			newTransform.scaleAboutPoint(scaleX, 1, cameraLayersBounds.getCenterX(), cameraViewBounds.getY());
		}

		camera.animateViewToTransform(newTransform, 0);
	}
}
