package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.CanvasSupplier;

import edu.umd.cs.piccolo.PCamera;

public class ZoomInAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 1051314263694073308L;

	@Override
	public void actionPerformed(ActionEvent e) {
		final PCamera camera = CanvasSupplier.getInstance().getCanvas().getCamera(); 
		camera.scaleViewAboutPoint(2, 1, camera.getViewBounds().getX(), 0);
	}
}
