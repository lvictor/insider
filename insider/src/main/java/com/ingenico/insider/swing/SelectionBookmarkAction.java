package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.SelectionControlSupplier;

public class SelectionBookmarkAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 6042645400402552351L;

	@Override
	public void actionPerformed(ActionEvent e) {
		SelectionControlSupplier.getInstance().addSelectionToMarks();
	}
}
