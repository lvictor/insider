package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;
import java.awt.geom.Rectangle2D;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.CanvasSupplier;

import edu.umd.cs.piccolo.PCamera;
import edu.umd.cs.piccolo.util.PAffineTransform;
import edu.umd.cs.piccolo.util.PBounds;
import edu.umd.cs.piccolo.util.PDimension;

public class ZoomHeightAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 3033903618541450956L;

	@Override
	public void actionPerformed(ActionEvent e) {
		final PCamera camera = CanvasSupplier.getInstance().getCanvas().getCamera(); 
		final Rectangle2D cameraLayersBounds = camera.getUnionOfLayerFullBounds();

		final PBounds cameraViewBounds = camera.getViewBounds();
		final PDimension delta = cameraViewBounds.deltaRequiredToCenter(cameraLayersBounds);
		final PAffineTransform newTransform = camera.getViewTransform();

		newTransform.translate(0, delta.height);

		final double scaleY = cameraViewBounds.getHeight() / cameraLayersBounds.getHeight();
		if (scaleY != Double.POSITIVE_INFINITY && scaleY != 0) {
			newTransform.scaleAboutPoint(1, scaleY, cameraViewBounds.getX(), cameraLayersBounds.getCenterY());
		}

		camera.animateViewToTransform(newTransform, 0);
	}
}
