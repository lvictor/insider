package com.ingenico.insider.swing;


import java.awt.Color;
import javax.swing.colorchooser.DefaultColorSelectionModel;

/**
 * Special ColorSelectionModel that supports the alpha channel.
 * It is supposed to be used in conjunction with special RGBAChooserPanel
 * (instead of swing DefaultRGBChooserPanel).
 *
 * @author Jean-Francois Poilpret
 */
public class AlphaColorSelectionModel extends DefaultColorSelectionModel
{
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -1979273492547313046L;

	public AlphaColorSelectionModel() {
		
	}
	
	public AlphaColorSelectionModel (Color c) {
		super (c);
		
		_alpha = c.getAlpha();
	}
	
	/**
	 * Allows to explicitely set the alpha channel of the color (transparency).
	 * This is the only way to set the alpha (do not use
	 * <code>setSelectedColor</code> it will ignore the alpha channel of its
	 * <code>color</code> argument).
	 */
	public void	setAlpha(int alpha, boolean refresh)
	{
		_alpha = alpha;
		if (refresh)
		{
			// The following line enforces update of the color in the parent
			// class and notification of all listeners.
			setSelectedColor(getSelectedColor());
		}
	}
	
	// Overridden
	public void setSelectedColor(Color color)
	{
		super.setSelectedColor(new Color(	color.getRed(),
											color.getGreen(),
											color.getBlue(),
											_alpha));
	}

	private int	_alpha = 0xFF;
}
