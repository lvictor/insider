package com.ingenico.insider.swing.table;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellEditor;

import com.ingenico.insider.swing.JAlphaColorChooser;

public class ColorCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener, MouseListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Color currentColor;

	Border unselectedBorder = null;
    Border selectedBorder = null;

    boolean isBordered = true;

    JLabel button;
	JColorChooser colorChooser;
	JDialog dialog;
	protected static final String EDIT = "edit";

	public ColorCellEditor(boolean isBordered) {
		button = new JLabel();
		button.setOpaque(true);
        this.isBordered = isBordered;
		button.addMouseListener(this);

//		Set up the dialog that the button brings up.
		colorChooser = new JAlphaColorChooser();
		dialog = JColorChooser.createDialog(button,
				"Pick a Color",
				true,  //modal
				colorChooser,
				this,  //OK button handler
				null); //no CANCEL button handler
	}
	
	public void actionPerformed(ActionEvent e) {
		currentColor = colorChooser.getColor();
	}
	
//	Implement the one CellEditor method that AbstractCellEditor doesn't.
	public Object getCellEditorValue() {
		return currentColor;
	}
	
//	Implement the one method defined by TableCellEditor.
	public Component getTableCellEditorComponent(JTable table, Object color, boolean isSelected, int row, int column) {
        currentColor = (Color)color;

        button.setBackground(currentColor);

        if (isBordered) {
            if (isSelected) {
                if (selectedBorder == null) {
                    selectedBorder = BorderFactory.createMatteBorder(1,1,1,1, table.getSelectionBackground());
                }
                button.setBorder(selectedBorder);
            } else {
                if (unselectedBorder == null) {
                    unselectedBorder = BorderFactory.createMatteBorder(1,1,1,1, table.getBackground());
                }
                button.setBorder(unselectedBorder);
            }
        }

        button.setToolTipText("RGBa value: (" + currentColor.getRed()   + ", "
                                              + currentColor.getGreen() + ", "
                                              + currentColor.getBlue()  + "), "
                                              + currentColor.getAlpha());
        return button;
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			colorChooser.setColor(currentColor);
			dialog.setVisible(true);

			fireEditingStopped(); //Make the renderer reappear.
		}
	}

	public void mousePressed(MouseEvent e)  {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseEntered(MouseEvent e)  {}
	public void mouseExited(MouseEvent e)   {}
}

