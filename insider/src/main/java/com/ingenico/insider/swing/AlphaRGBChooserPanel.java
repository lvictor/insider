package com.ingenico.insider.swing;


import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import javax.swing.colorchooser.ColorSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class AlphaRGBChooserPanel extends AbstractColorChooserPanel {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -5315510394031352204L;

	protected final ChangeListener	_listener = new ValueListener();
	protected final JSlider[]		_rgbaSliders = new JSlider[4];
	protected final JSpinner[]		_rgbaSpinners = new JSpinner[4];
	private boolean					_isAdjusting = false;

	static final private int	RED		= 0;
	static final private int	GREEN	= 1;
	static final private int	BLUE	= 2;
	static final private int	ALPHA	= 3;
	static final private int	MIN_VALUE = 0;
	static final private int	MAX_VALUE = 255;

	private class ValueListener implements ChangeListener
	{
		public void stateChanged(ChangeEvent e)
		{
			if (_isAdjusting)
				return;
			int red, green, blue, alpha;
			if (e.getSource() instanceof JSlider)
			{
				red = _rgbaSliders[RED].getValue();
				green = _rgbaSliders[GREEN].getValue();
				blue = _rgbaSliders[BLUE].getValue() ;
				alpha = _rgbaSliders[ALPHA].getValue() ;
			}
			else
			{
				red = ((Integer) _rgbaSpinners[RED].getValue()).intValue();
				green = ((Integer) _rgbaSpinners[GREEN].getValue()).intValue();
				blue = ((Integer) _rgbaSpinners[BLUE].getValue()).intValue();
				alpha = ((Integer) _rgbaSpinners[ALPHA].getValue()).intValue();
			}
			ColorSelectionModel model = getColorSelectionModel();
			if (model instanceof AlphaColorSelectionModel)
			{
				((AlphaColorSelectionModel) model).setAlpha(alpha, false);
			}
			model.setSelectedColor(new Color(red, green, blue));
		}
	}
	
	protected void	setColor(Color newColor)
	{
		setColor(RED, newColor.getRed());
		setColor(GREEN, newColor.getGreen());
		setColor(BLUE, newColor.getBlue());
		setColor(ALPHA, newColor.getAlpha());
	}
	
	protected void	setColor(int row, int value)
	{
		if (_rgbaSliders[row].getValue() != value)
		{
			_rgbaSliders[row].setValue(value);
		}
		if (((Integer) _rgbaSpinners[row].getValue()).intValue() != value)
		{
			_rgbaSpinners[row].setValue(new Integer(value));
		}
	}

	@Override
	public void updateChooser() {
		if (!_isAdjusting)
		{
			_isAdjusting = true;
			setColor(getColorFromModel());
			_isAdjusting = false;
		}
	}

	@Override
	protected void buildChooser() {
		Color color = getColorFromModel();

		setLayout(new GridBagLayout());

		// Build each color channel row: R,G,B,A
		buildRow(	RED, 
					"ColorChooser.rgbRedText", 
					"ColorChooser.rgbRedMnemonic", 
					color.getRed());
		buildRow(	GREEN, 
					"ColorChooser.rgbGreenText", 
					"ColorChooser.rgbGreenMnemonic", 
					color.getGreen());
		buildRow(	BLUE, 
					"ColorChooser.rgbBlueText", 
					"ColorChooser.rgbBlueMnemonic", 
					color.getBlue());
		buildRow(	ALPHA, 
					"Alpha", 
					(int) 'A', 
					color.getAlpha());
	}

	protected void	buildRow(int row, String label, String mnemonic, int value)
	{
		buildRow(row, UIManager.getString(label), UIManager.getInt(mnemonic), value);
	}

	protected void	buildRow(int row, String label, int mnemonic, int value)
	{
		// Create the widgets
		JLabel rowLabel = new JLabel(label);
		rowLabel.setDisplayedMnemonic(mnemonic);
		JSlider slider = new JSlider(JSlider.HORIZONTAL, MIN_VALUE, MAX_VALUE, value);
		slider.setMajorTickSpacing(85);
		slider.setMinorTickSpacing(17);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.addChangeListener(_listener);
		slider.putClientProperty("JSlider.isFilled", Boolean.TRUE);
		JSpinner spinner = new JSpinner(
								new SpinnerNumberModel(value, MIN_VALUE, MAX_VALUE, 1));
		spinner.addChangeListener(_listener);

		_rgbaSliders[row] = slider;
		_rgbaSpinners[row] = spinner;
		
		// Add the widgets to the panel
		Insets insets = new Insets(2, 2, 2, 2);
		GridBagConstraints constraints = new GridBagConstraints(0, row, 1, 1,
																0.0, 0.0,
																GridBagConstraints.WEST,
																GridBagConstraints.HORIZONTAL,
																insets,
																0, 0);
		add(rowLabel, constraints);

		constraints.gridx = 2;
		add(spinner, constraints);

		constraints.gridx = 1;
		constraints.weightx = 1.0;
		add(slider, constraints);
	}

	@Override
	public String getDisplayName() {
		return UIManager.getString("ColorChooser.rgbNameText");
	}

	@Override
	public Icon getSmallDisplayIcon() {
		return null;
	}

	@Override
	public Icon getLargeDisplayIcon() {
		return null;
	}

}
