package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.CanvasSupplier;

import edu.umd.cs.piccolo.PCamera;

public class ZoomOutAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -7624959994110235045L;

	@Override
	public void actionPerformed(ActionEvent e) {
		PCamera camera = CanvasSupplier.getInstance().getCanvas().getCamera();

		// set the zoom to half the current value.
		camera.scaleViewAboutPoint(0.5, 1, camera.getViewBounds().getX(), 0);
	}
}
