package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.SelectionControlSupplier;

public class MarkDeleteAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 3747320162112178088L;

	@Override
	public void actionPerformed(ActionEvent e) {
		SelectionControlSupplier.getInstance().deleteMark();
	}
}
