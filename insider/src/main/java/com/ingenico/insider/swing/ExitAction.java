package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.DockableViewsSupplier;


public class ExitAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -6207625432969353493L;

	@Override
	public void actionPerformed(ActionEvent e) {
		DockableViewsSupplier.getInstance().close();
	}
}
