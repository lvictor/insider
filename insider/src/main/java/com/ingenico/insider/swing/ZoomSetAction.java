package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.CanvasSupplier;
import com.ingenico.insider.services.impl.MessageBoxSupplier;
import com.ingenico.insider.services.impl.ZoomControlSupplier;

import edu.umd.cs.piccolo.PCamera;

public class ZoomSetAction extends AbstractAction {
	private static final Logger _logger = Logger.getLogger(ZoomSetAction.class.getCanonicalName()); 

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 1145839545461906972L;

	@Override
	public void actionPerformed(ActionEvent e) {
		final PCamera camera = CanvasSupplier.getInstance().getCanvas().getCamera();
		final String scaleXText = ZoomControlSupplier.getInstance().getScaleXText();
		final String scaleYText = ZoomControlSupplier.getInstance().getScaleYText();
		try {
			final double cameraXScale = camera.getViewScaleX();
			final double cameraYScale = camera.getViewScaleY();
			final double xScale = Double.parseDouble(scaleXText) / cameraXScale;
			final double yScale = Double.parseDouble(scaleYText) / cameraYScale;
			if (_logger.isLoggable(Level.FINEST)) {
				_logger.finest("Target View Scale X = " + scaleXText + ", Camera View Scale X = " + cameraXScale + ", Scale Factor Applied = " + xScale);
				_logger.finest("Target View Scale Y = " + scaleYText + ", Camera View Scale Y = " + cameraYScale + ", Scale Factor Applied = " + yScale);
			}
			camera.scaleViewAboutPoint(xScale, yScale, camera.getViewBounds().getX(), camera.getViewBounds().getY());
		} catch (NumberFormatException nfe) {
			MessageBoxSupplier.getInstance().showErrorDialog(nfe.getClass().getSimpleName(), scaleXText);
		}
	}
}
