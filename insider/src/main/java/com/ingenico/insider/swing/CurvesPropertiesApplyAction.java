package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;
import java.util.logging.Logger;

import javax.swing.AbstractAction;

import com.ingenico.insider.nodes.PSampleChannel;
import com.ingenico.insider.services.impl.CurvePropertyControlSupplier;
import com.ingenico.insider.services.impl.CurvesControlSupplier;

public class CurvesPropertiesApplyAction extends AbstractAction {
	private static final Logger _logger = Logger.getLogger(CurvesPropertiesApplyAction.class.getCanonicalName());

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -3961983637483912739L;

	@Override
	public void actionPerformed(ActionEvent e) {
		final Object[] selectedCurves = CurvesControlSupplier.getInstance().getSelectedValues();
		for (int i = 0; i < selectedCurves.length; i++) {
			PSampleChannel currentCurve = (PSampleChannel) selectedCurves[i];

			_logger.finest("Applying Properties to " + currentCurve);
			CurvePropertyControlSupplier.getInstance().setPropertiesToPSampleChannel(currentCurve);
		}
	}
}
