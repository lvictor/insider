package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.LayoutControlSupplier;


public class LayoutLoadAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 6891011380647017154L;

	@Override
	public void actionPerformed(ActionEvent e) {
		LayoutControlSupplier.getInstance().load();
	}
}
