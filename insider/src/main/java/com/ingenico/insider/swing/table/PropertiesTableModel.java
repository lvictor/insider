package com.ingenico.insider.swing.table;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

/**
 * This model behaves exactly like the DefaultTableModel
 * except that the first column is not editable...
 *
 * @author lvictor
 */
public class PropertiesTableModel extends DefaultTableModel {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3017985281753072016L;

	public PropertiesTableModel() {
        super();
    }

    public PropertiesTableModel(int rowCount, int columnCount) {
    	super(rowCount, columnCount);
    }

    public PropertiesTableModel(Vector columnNames, int rowCount) {
    	super(columnNames, rowCount);
    }

    public PropertiesTableModel(Object[] columnNames, int rowCount) {
    	super(columnNames, rowCount);
    }
    
    public PropertiesTableModel(Vector data, Vector columnNames) {
    	super(data, columnNames);
    }
    
    public PropertiesTableModel(Object[][] data, Object[] columnNames) {
    	super(data, columnNames);
    }

    public boolean isCellEditable(int row, int column) {
    	if (column < 1){
    		return false;
    	} else {
    		return true;
    	}
    }
}
