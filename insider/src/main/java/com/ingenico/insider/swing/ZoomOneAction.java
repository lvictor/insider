package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.CanvasSupplier;

public class ZoomOneAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -6918504103636625273L;

	@Override
	public void actionPerformed(ActionEvent e) {
		CanvasSupplier.getInstance().getCanvas().getCamera().setViewScale(1, 1);
	}
}
