package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.SelectionControlSupplier;

public class MarkGotoAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 6595059436918271183L;

	@Override
	public void actionPerformed(ActionEvent e) {
		SelectionControlSupplier.getInstance().gotoMark();
	}

}
