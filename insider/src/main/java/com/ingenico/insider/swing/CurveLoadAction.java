package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.CurvesControlSupplier;


public class CurveLoadAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 2835757541082391513L;

	@Override
	public void actionPerformed(ActionEvent e) {
		CurvesControlSupplier.getInstance().load();
	}
}
