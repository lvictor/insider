package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.CurvesControlSupplier;


public class CurveSaveAsAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -6973182393080017566L;

	@Override
	public void actionPerformed(ActionEvent e) {
		CurvesControlSupplier.getInstance().saveAs();
	}
}
