package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.nodes.PMark;
import com.ingenico.insider.services.impl.CanvasSupplier;
import com.ingenico.insider.services.impl.SelectionControlSupplier;

import edu.umd.cs.piccolo.util.PBounds;

public class MarkCameraAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -8784290227700920271L;

	@Override
	public void actionPerformed(ActionEvent e) {
		final PMark mark = new PMark();
		final PBounds viewBounds = CanvasSupplier.getInstance().getCanvas().getCamera().getViewBounds();

		mark.setBounds(viewBounds);
		SelectionControlSupplier.getInstance().addMark(mark);
	}

}
