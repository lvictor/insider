package com.ingenico.insider.swing;


import java.awt.Color;
import java.awt.Component;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.colorchooser.AbstractColorChooserPanel;

public class JAlphaColorChooser extends JColorChooser {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates a color chooser pane with an initial color of white.
	 */
	public JAlphaColorChooser () {
		this(Color.white);
	}
	
	/**
	 * Creates a color chooser pane with the specified initial color.
	 *
	 * @param initialColor the initial color set in the chooser
	 */
	public JAlphaColorChooser(Color initialColor) {
		this( new AlphaColorSelectionModel(initialColor) );
		
	}
	
	/**
	 * Creates a color chooser pane with the specified
	 * <code>AlphaColorSelectionModel</code>.
	 *
	 * @param model the <code>ColorSelectionModel</code> to be used
	 */
	public JAlphaColorChooser(AlphaColorSelectionModel model) {
		super (model);
		
		// Browse available panels in the default dialog box and remove the default RGB panel (if found)
		AbstractColorChooserPanel [] availablePanels = getChooserPanels();;
		for (int i = 0; i < availablePanels.length; i++) {
			if ("javax.swing.colorchooser.DefaultRGBChooserPanel".equals((availablePanels[i]).getClass().getName())) {
				removeChooserPanel(getChooserPanels()[i]);
			}
		}

		// Then add the RGBA panel
		addChooserPanel (new AlphaRGBChooserPanel());
	}
	
	/**
	 * Shows a modal color-chooser dialog and blocks until the
	 * dialog is hidden.  If the user presses the "OK" button, then
	 * this method hides/disposes the dialog and returns the selected color.
	 * If the user presses the "Cancel" button or closes the dialog without
	 * pressing "OK", then this method hides/disposes the dialog and returns
	 * <code>null</code>.
	 *
	 * @param component    the parent <code>Component</code> for the dialog
	 * @param title        the String containing the dialog's title
	 * @param initialColor the initial Color set when the color-chooser is shown
	 * @return the selected color or <code>null</code> if the user opted out
	 * @exception HeadlessException if GraphicsEnvironment.isHeadless()
	 * returns true.
	 * @see java.awt.GraphicsEnvironment#isHeadless
	 */
	public static Color showDialog(Component component, String title, Color initialColor)
		throws HeadlessException
	{
		final JAlphaColorChooser pane = new JAlphaColorChooser(initialColor != null ? initialColor : Color.white);
		
		ColorTracker ok = new ColorTracker(pane);
		JDialog dialog = createDialog(component, title, true, pane, ok, null);
		
		dialog.setVisible(true); // blocks until user brings dialog down...
		
		return ok.getColor();
	}
	
}

class ColorTracker implements ActionListener, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	JColorChooser chooser;
	Color color;
	
	public ColorTracker(JColorChooser c) {
		chooser = c;
	}
	
	public void actionPerformed(ActionEvent e) {
		color = chooser.getColor();
	}
	
	public Color getColor() {
		return color;
	}
}