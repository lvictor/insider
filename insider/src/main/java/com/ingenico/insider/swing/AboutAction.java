package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;

import com.ingenico.insider.services.impl.MessageBoxSupplier;
import com.ingenico.piccolo.nodes.PAboutCanvas;


public class AboutAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -6207625432969353493L;

	private static final PAboutCanvas help;

	private Object[] message;

	@Override
	public void actionPerformed(ActionEvent e) {
		final Object message = getValue(Action.LONG_DESCRIPTION);
		if (message != null) {
			this.message = new Object[] {new JLabel((String)message, JLabel.CENTER), help};
		} else {
			this.message = new Object[] {help};
		}

		help.initActivity();
		MessageBoxSupplier.getInstance().showOptionDialog(
        	this.message,
        	(String)AboutAction.this.getValue(Action.NAME),
        	JOptionPane.CLOSED_OPTION,
        	JOptionPane.PLAIN_MESSAGE,
        	(Icon)AboutAction.this.getValue(Action.SMALL_ICON),
        	null,
        	null
        );
        help.shutdownActivity();
	}

	static {
		help = new PAboutCanvas(new String[] {
			"insider\n         (c)ingenico",
			"code by:\n         Lionel VICTOR",
			"Zoomable interface engine:\n            piccoloXY",
			"Docking windows framework:\n            infonode",
			"   special thanks to:\nOlivier BENOIT"
		});
		help.setBorder(LineBorder.createGrayLineBorder());

	}
}
