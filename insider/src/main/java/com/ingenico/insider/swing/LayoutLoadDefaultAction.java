package com.ingenico.insider.swing;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.ingenico.insider.services.impl.LayoutControlSupplier;


public class LayoutLoadDefaultAction extends AbstractAction {
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -3058926925779580097L;

	@Override
	public void actionPerformed(ActionEvent e) {
		LayoutControlSupplier.getInstance().loadDefault();
	}
}
