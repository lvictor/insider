package com.ingenico.insider.docking;

import java.awt.Component;

import net.infonode.docking.View;

public class DynamicView extends View {
	/**
	 * Generated serialVersionUID for serialization
	 */
	private static final long serialVersionUID = -3289524484105010742L;

	private final int viewID;

	private static final class UIDGenerator {
		private static int currentID = 0;

		private static final int generateUID() {
			return currentID++;
		}
	}

	/**
	 * Build a Dynamic View.
	 * 
	 * Dynamic views embeds their own auto-generated identifier. The constructor is
	 * therefore similar to the one of the View.
	 * 
	 * @param title the title of the view
	 * @param description the description that will be stored in the view's tool tip text
	 * @param icon the icon for the view
	 * @param component the component to place inside the view
	 */
	public DynamicView(Component component) {
		super(null, null, component);
		viewID = UIDGenerator.generateUID();
	}

	/**
	 * Returns the view id.
	 *
	 * @return the view id
	 */
	public int getViewId() {
		return viewID;
	}
}
