package com.ingenico.insider.model;

import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

public interface PCurveListDataListener extends ListDataListener {

    /**
     * Sent after the indices in the index0,index1 interval
     * have been removed from the data model.  The interval 
     * includes both index0 and index1.
     *
     * @param e  a <code>ListDataEvent</code> encapsulating the
     *    event information
     */
    void intervalMoved(ListDataEvent e);

}
