package com.ingenico.tools.data.sampling;

import com.ingenico.tools.nio.SampleBuffer;

public class NaiveDoublePathSampler extends AbstractDiscretePathSampler {
	@Override
	protected double getDoubleValue (SampleBuffer buf) {
		return buf.get().doubleValue();
	}
}
