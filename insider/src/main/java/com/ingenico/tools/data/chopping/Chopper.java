package com.ingenico.tools.data.chopping;

import java.awt.geom.Rectangle2D;
import java.util.Set;

public interface Chopper {
	public Set<Rectangle2D> getChunkSet (Rectangle2D samplesWindowBounds, int targetLength);
}
