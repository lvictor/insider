package com.ingenico.tools.data.sampling;

import com.ingenico.tools.nio.SampleBuffer;

public class MaxDoublePathSampler extends AbstractDiscretePathSampler {
	@Override
	protected double getDoubleValue (SampleBuffer buf) {
		final int bufferLimit = buf.limit();
		double max = Double.NEGATIVE_INFINITY;

		for (int i = 0; i < bufferLimit; i++) {
			double current = buf.get().doubleValue();
			if (current > max) {
				max = current;
			}
		}

		return max;
	}
}
