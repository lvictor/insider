package com.ingenico.tools.data.sampling;

import java.awt.geom.Path2D;
import java.io.IOException;

import com.ingenico.tools.nio.channel.RandomReadableSampleChannel;

public interface PathSampler {
	public Path2D sampleToPath (RandomReadableSampleChannel byteChannel, double scale, int targetLength, long offset) throws IOException;
}
