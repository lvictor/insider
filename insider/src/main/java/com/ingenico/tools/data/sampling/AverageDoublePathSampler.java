package com.ingenico.tools.data.sampling;

import com.ingenico.tools.nio.SampleBuffer;

public class AverageDoublePathSampler extends AbstractDiscretePathSampler {
	@Override
	protected double getDoubleValue (SampleBuffer buf) {
		final int bufferLimit = buf.limit();
		double sum = 0;

		for (int i = 0; i < bufferLimit; i++) {
			sum += buf.get().doubleValue();
		}

		return (double)(sum/bufferLimit);
	}
}
