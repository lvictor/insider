package com.ingenico.tools.data.sampling;

import com.ingenico.tools.nio.SampleBuffer;

public class MinDoublePathSampler extends AbstractDiscretePathSampler {
	@Override
	protected double getDoubleValue (SampleBuffer buf) {
		final int bufferLimit = buf.limit();
		double min = Double.POSITIVE_INFINITY;

		for (int i = 0; i < bufferLimit; i++) {
			double current = buf.get().doubleValue();
			if (current < min) {
				min = current;
			}
		}

		return min;
	}
}
