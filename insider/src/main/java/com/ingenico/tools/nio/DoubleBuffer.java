package com.ingenico.tools.nio;

public class DoubleBuffer extends SampleBuffer {
	public DoubleBuffer(java.nio.ByteBuffer buffer) {
		super(buffer, 8);
	}

	@Override
	public Number get() {
		Number pipo = buffer.getDouble();
		return pipo;
	}
}
