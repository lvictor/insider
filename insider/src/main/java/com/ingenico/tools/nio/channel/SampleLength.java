package com.ingenico.tools.nio.channel;

public final class SampleLength {
	private final String name;
	private final int length;

	public final static SampleLength BYTE   = new SampleLength("8 bits", 1);
	public final static SampleLength SHORT  = new SampleLength("16 bits", 2);
	public final static SampleLength FLOAT = new SampleLength("float", 4);
	public final static SampleLength DOUBLE = new SampleLength("double", 8);

	private SampleLength(String name, int length) {
		this.name = name;
		this.length = length;
	}

	public int getLength() {
		return length;
	}

	public final String toString() {
		return name;
	}
}
