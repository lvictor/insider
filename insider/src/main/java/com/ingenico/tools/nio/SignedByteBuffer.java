package com.ingenico.tools.nio;

public class SignedByteBuffer extends SampleBuffer {
	public SignedByteBuffer(java.nio.ByteBuffer buffer) {
		super(buffer, 1);
	}

	@Override
	public Number get() {
		return buffer.get();
	}
}
