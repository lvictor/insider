package com.ingenico.tools.nio.channel;

import java.io.IOException;

import com.ingenico.tools.nio.SampleBuffer;

public interface RandomReadableSampleChannel extends RandomAccessChannel, ReadableSampleChannel {
	/**
	 * Reads a sequence of bytes from this channel into the given buffer, starting at the given file position.
	 *
	 * @param dst The buffer into which bytes are to be transferred
	 * @param position The file position at which the transfer is to begin; must be non-negative
	 * @return The number of bytes read, possibly zero, or -1 if the given position is greater than or equal to the file's current size
	 * @throws IOException
	 */
	int read(SampleBuffer dst, long position) throws IOException;

	SampleBuffer allocate(int samplesWindowLength);
}
