package com.ingenico.tools.nio.channel;

import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import com.ingenico.tools.nio.SampleBuffer;

public abstract class SampleFileChannel implements RandomReadableSampleChannel {
	protected FileChannel channel;
	protected int sampleRate;

	public final static SampleFileChannel createSampleFileChannel(final File file, final SampleLength sampleLength, final SampleSign sign, final ByteOrder order, int sampleRate) throws IOException {
		SampleFileChannel sampleChannel;

		if (SampleLength.BYTE == sampleLength) {
			// Order has no meaning for bytes...
			sampleChannel = new ByteFileChannel(file, sign);
		} else if (SampleLength.SHORT == sampleLength) {
			sampleChannel = new ShortFileChannel(file, sign, order);
		} else if (SampleLength.FLOAT == sampleLength) {
			sampleChannel = new FloatFileChannel(file, sign, order);
		} else if (SampleLength.DOUBLE == sampleLength) {
			sampleChannel = new DoubleFileChannel(file, sign, order);
		} else {
			throw new NotImplementedException();
		}

		sampleChannel.setSampleRate(sampleRate);
		return sampleChannel;
	}

	public void setSampleRate(int sampleRate) {
		this.sampleRate = sampleRate;
	}

	@Override
	public int getSampleRate() throws IOException {
// TODO Auto-generated method stub
		return sampleRate;
	}

	@Override
	public void close() throws IOException {
		channel.close();
	}

	@Override
	public boolean isOpen() {
		return channel.isOpen();
	}

	@Override
	public abstract int read(final SampleBuffer dst) throws IOException;

	public abstract int read(final SampleBuffer dst, long position) throws IOException;

	public abstract RandomAccessChannel position(long newPosition) throws IOException;

	public abstract long position() throws IOException;
}
