package com.ingenico.tools.nio;

import java.nio.ByteBuffer;

public class SignedShortBuffer extends SampleBuffer {
	public SignedShortBuffer(ByteBuffer buffer) {
		super(buffer, 2);
	}

	@Override
	public Number get() {
		Number pipo = buffer.getShort();
		return pipo;
	}
}
