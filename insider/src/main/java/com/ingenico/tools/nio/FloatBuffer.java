package com.ingenico.tools.nio;

public class FloatBuffer extends SampleBuffer {
	public FloatBuffer(java.nio.ByteBuffer buffer) {
		super(buffer, 4);
	}

	@Override
	public Number get() {
		Number pipo = buffer.getFloat();
		return pipo;
	}
}
