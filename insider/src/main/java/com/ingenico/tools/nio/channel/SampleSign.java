package com.ingenico.tools.nio.channel;

public class SampleSign {
	private final String name;

	public final static SampleSign SIGNED  = new SampleSign("signed");
	public final static SampleSign UNSIGNED  = new SampleSign("unsigned");

	private SampleSign(String name) {
		this.name = name;
	}

	public final String toString() {
		return name;
	}
}
