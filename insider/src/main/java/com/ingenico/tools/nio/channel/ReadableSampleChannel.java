package com.ingenico.tools.nio.channel;

import java.io.IOException;
import java.nio.channels.Channel;

import com.ingenico.tools.nio.SampleBuffer;

public interface ReadableSampleChannel extends Channel {
    public int read(SampleBuffer dst) throws IOException;
	public int getSampleRate() throws IOException;
}
