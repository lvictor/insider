package com.ingenico.tools.nio.channel;

import java.io.IOException;

public interface RandomAccessChannel {
	public long position () throws IOException;
	public RandomAccessChannel position (long position) throws IOException;
	public long size() throws IOException;
}
