package com.ingenico.tools.nio;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public abstract class SampleBuffer {
	protected final ByteBuffer buffer;
	protected final int sampleSize;

	protected SampleBuffer(ByteBuffer buffer, int sampleSize) {
		this.buffer = buffer;
		this.sampleSize = sampleSize;
	}

	public final ByteOrder order() {
		return buffer.order();
	}

	public final SampleBuffer order(ByteOrder bo) {
		buffer.order(bo);
		return this;
	}

    public final int capacity(){
    	return buffer.capacity() / sampleSize;
    }

    public final int position() {
    	return buffer.position() / sampleSize;
    }
    
    public final Buffer position(int newPosition) {
    	return buffer.position(newPosition * sampleSize);
    }

    public final int limit() {
    	return buffer.limit() / sampleSize;
    }

    public final Buffer limit(int newLimit) {
    	return buffer.limit(newLimit * sampleSize);
    }

    public final Buffer mark() {
    	return buffer.mark();
    }

    public final Buffer reset() {
    	return buffer.reset();
    }
    
    public final Buffer clear() {
    	return buffer.clear();
    }
    
    public final Buffer flip() {
    	return buffer.flip();
    }
    
    public final Buffer rewind() {
    	return buffer.rewind();
    }

    public final int remaining() {
    	return buffer.remaining() / sampleSize;
    }
    
    public final boolean hasRemaining() {
    	return buffer.hasRemaining();
    }
    
    public final boolean isReadOnly() {
    	return buffer.isReadOnly();
    }

    public final boolean hasArray() {
    	return buffer.hasArray();
    }
    
    public final Object array() {
    	return buffer.array();
    }

    public final int arrayOffset() {
    	return buffer.arrayOffset();
    }

    public final boolean isDirect() {
    	return buffer.isDirect();
    }

    public final ByteBuffer toByteBuffer() {
    	return buffer;
    }
    
	public abstract Number get();
}
