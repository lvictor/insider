package com.ingenico.tools.nio;

public class ByteBuffer extends SampleBuffer {
	public ByteBuffer(java.nio.ByteBuffer buffer) {
		super(buffer, 1);
	}

	@Override
	public Number get() {
		return (buffer.get() & 0xFF);
	}
}
