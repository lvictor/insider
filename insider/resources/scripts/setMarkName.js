// Open a dialog box and asks for a string that will be used to rename
// selected marks.
// if the used inputs "{#}" in the string, then this pattern will be replaced
// with the item number starting with 0.

//importClass(com.ingenico.insider.services.impl.SelectionLayerSupplier);
importClass(com.ingenico.insider.services.impl.SelectionControlSupplier);
importClass(com.ingenico.insider.nodes.PMark);
importClass(javax.swing.JOptionPane);

//var mark = SelectionLayerSupplier.getInstance().getModel().getElementAt(0);

var marks = SelectionControlSupplier.getInstance().getSelectedValues();
var i = 0;

var s = JOptionPane.showInputDialog(
		null,
		"Insert the mark name:\n(the pattern \"{#}\" will be replaced with the selection index)",
		"Set selected marks names...",
		JOptionPane.PLAIN_MESSAGE,
		null,
		null,
		"sample {#}"
);

//If a string was returned, say so.
if ((s != null) && (s.length() > 0)) {
	for(i=0; i < marks.length; i++) {
		var mark = marks[i];
		var markText = s.replace("{#}", i+"");
		mark.setMarkName(markText);
	}
}