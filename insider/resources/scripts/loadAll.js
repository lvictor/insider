// This script loads an image then moves it around in front of the camera

importClass(java.io.File);
importClass(com.ingenico.insider.services.impl.CurvesSupplier);

importClass(com.ingenico.tools.nio.channel.SampleLength);
importClass(com.ingenico.tools.nio.channel.SampleSign);
importClass(java.nio.ByteOrder);

////////////////////////////////////////////////////////////////////////////////
// COMPLETE INFORMATIONAL VALUES BETWEEN SLASHES
////////////////////////////////////////////////////////////////////////////////
// Values for the KEY (Loaded after all other values)
var key = [0,1,2,3,4,5,6,7,8,9,230,231,232, 233, 234, 0xFF];

// SampleLength.BYTE
// SampleLength.SHORT
var sampleLength = SampleLength.BYTE;

// SampleSign.SIGNED
// SampleSign.UNSIGNED
var sampleSign = SampleSign.SIGNED;

// ByteOrder.BIG_ENDIAN
// ByteOrder.LITTLE_ENDIAN
var byteOrder = ByteOrder.BIG_ENDIAN

// Prefix directory without trailing slash... Do not forget to escape backslashes under windows
var prefixDir = "E:\\tmp\\glurp";
////////////////////////////////////////////////////////////////////////////////
// END OF USER CONTRIB HERE... JUST LAUNCH THE SCRIPT... AND LOOK THE DOS WINDOW FOR OUTPUT
////////////////////////////////////////////////////////////////////////////////


for(s=0; s <= 15; s++) {
  var sStr = ""+s;
  if (s < 10) {
    sStr = "0"+s;
  }

  for (c=0; c <= 255; c++) {
    var cStr = ""+c;
    if (c < 10) {
      cStr = "00"+c;
    } else if (c < 100) {
      cStr = "0"+c;
    }

    var fileName = "E:\\tmp\\glurp\\S"+ sStr + "\\S" + sStr + ".K" + cStr;
    var currentFile = new File(fileName);

    if (c == key[s]) {
      java.lang.System.out.println ("skipping " + fileName);
      continue;
    }

    if (currentFile.isFile()) {
      CurvesSupplier.getInstance().loadRawFile(currentFile, sampleLength, sampleSign, byteOrder, 0);
      java.lang.System.out.println ("Loading: " + fileName);
    } else {
      java.lang.System.out.println ("File does not exists: " + fileName);
    }
  }
}

for(s=0; s <= 15; s++) {
  var sStr = ""+s;
  if (s < 10) {
    sStr = "0"+s;
  }

  var cStr = ""+key[s];
  if (c < 10) {
    cStr = "00"+c;
  } else if (c < 100) {
    cStr = "0"+c;
  }

  var fileName = prefixDir + "\\S"+ sStr + "\\S" + sStr + ".K" + cStr;
  var currentFile = new File(fileName);

  if (currentFile.isFile()) {
    CurvesSupplier.getInstance().loadRawFile(currentFile, sampleLength, sampleSign, byteOrder, 0);
    java.lang.System.out.println ("Loading KEY: " + fileName);
  } else {
    java.lang.System.out.println ("KEY File does not exists: " + fileName);
  }
}