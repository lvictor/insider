// This script loads an image then moves it around in front of the camera

importClass(com.ingenico.insider.services.impl.CanvasSupplier);
importClass(edu.umd.cs.piccolo.nodes.PImage);
importClass(java.awt.Color);
importClass(javax.swing.ImageIcon);

// This would set the background color
CanvasSupplier.getInstance().getCanvas().setBackground(Color.GRAY);

// This loads an image and adds it under the camera (do not move with the canvas or the zoom)
var image = new PImage(new ImageIcon("D:/Users/lvictor/Mes documents/Mes images/ingenicoy.png").getImage());
CanvasSupplier.getInstance().getCanvas().getCamera().addChild(image);

// This animates the previously added image...
image.setRotation(5);
image.setScale(2);
image.animateToPositionScaleRotation(690, 320, 1, 0.0, 10000);
