importClass(com.ingenico.insider.services.impl.CurvesControlSupplier);

var curves = CurvesControlSupplier.getInstance().getSelectedValues();

// Initial Offset
var currentOffset = 200;

for(i=0; i < curves.length; i++) {
	var curve = curves[i];
	var curveOffsetX = curve.getOffset().getX();
  curve.setOffsetX(curveOffsetX + currentOffset);
  
  // Increment Value
  currentOffset += 100;
}
