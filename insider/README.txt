INTRODUCTION:
-------------
insider is a graphical tool that reads binary values from files and draws a discrete graphical representations of this data.

Typically, the file is interpreted as a list of discrete samples acquired synchroneously overtime. In insider's representation, the X axis represents the time while the Y axis represents the value of the sample (in time).

When drawing the content of a very large file, a computer screen does not have enough dots to completely represent each sample. One dot may therefore represent many samples at once. insder is built to offfer multiple methods for choosing how the displayed dot is elected among the many samples it represents.

insider is designed to work on multiple platforms and to be fast and efficient event when opening with multiple large files. It has also been designed with the ability to read different samples size and endianness.


BUILDING:
---------
The software builds on top of a modified version of the piccolo2d library and also uses Infonode Docking Windows (IDW)

For more information about piccolo2d, please visit http://www.piccolo2d.org/
For more information about IDW, please visit http://www.infonode.net/

insider build uses maven (http://maven.apache.org/). It should be fairly easily to build it with eclipse or any other IDE but this documentation will only focus on maven though.

first, you need to download Infonode Docking Windows and install the jar in your maven cache. It seems that IDW-GPL can not be found on any public repository.
$ wget http://freefr.dl.sourceforge.net/project/infonode/InfoNode%20Docking%20Windows/1.6.1/idw-gpl-1.6.1.zip

Once you have the file named idw-gpl.jar, use the command line to install it in your local cache:
$ mvn install:install-file -Dfile=idw-gpl-1.6.1/lib/idw-gpl.jar -DgroupId=net.infonode -DartifactId=idw-gpl -Dversion=1.6.1 -Dpackaging=jar

second, you need to download piccolo2d source code from its web site and apply a patch. For historical insider was initially based on a separated proprietary branch of Piccolo. While most features of this branch could be performed with latest versions of piccolo2d, a few functions are still missing and you must apply a patch to piccolo2d.

To apply the patch, please use the diff files provided in the piccolo2dxy module as follows:
Download and unzip the piccolo source code:
$ wget http://piccolo2d.googlecode.com/files/piccolo2d-1.3.1-src.zip
$ unzip piccolo2d-1.3.1-src.zip

Go to the piccolo2d source code folder you've just unzipped and try to apply the xy patch
$ cd piccolo2d-1.3.1-src
$ patch --dry-run -p1 < ../piccolo2dxy/src/main/patch/Piccolo2D-1.3.1.XY.patch

This must not return any error...
YOU MAY HAVE PROBLEMS WITH CR/LF characters... If patch reports errors like "Hunk #1 FAILED at 32." then try to convert them with dos2unix or unis2dos:
$ dos2unix parent/pom.xml

Plays with dos2unix and unix2dos until patch --dry-run reports a successful patching...
Now we can REALLY apply the patch:
$ patch -p1 < ../piccolo2dxy/src/main/patch/Piccolo2D-1.3.1.XY.patch


You are ready to build and install the patched library to your local maven cache.
$ mvn install

The resulting (patched) library is now installed in your maven cache under teh artifact ids piccolo2Dxy-core and piccolo2dxy-extras.

You can now build insider:
$ cd ../insider
$ mvn package

Hopefully, you will find your packaged zip in the target directory (file named insider-1.0-SNAPSHOT-insider-delivery.zip).

RUNNING:
--------

unzip the packaged zip in a folder of your choice.

go to the script subfolder and run one of insider.bat or insider.sh whether you are running under Windows or Unix respectively. MacOS is a unix OS and therefore, Mac users can run insider.sh.


YOU MAY HAVE PROBLEMS under Unix if the insider.sh script does not have execute permission. Simply chmod it:
$ chmod +x insider.sh
